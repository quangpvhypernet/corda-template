package com.template.webserver

import net.corda.client.rpc.CordaRPCClient
import net.corda.client.rpc.CordaRPCConnection
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.utilities.NetworkHostAndPort
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

interface NodeRPCConnection

interface RPCProxyOwner : AutoCloseable {
    val rpcConnection: CordaRPCConnection

    val proxy: CordaRPCOps get() = rpcConnection.proxy

    @PreDestroy
    override fun close() {
        rpcConnection.close()
    }
}

class MockNodeRPCConnection : NodeRPCConnection

class NodeRPCConnectionImpl(
        private val host: String,
        private val username: String,
        private val password: String,
        private val rpcPort: Int
) : NodeRPCConnection, RPCProxyOwner {

    override lateinit var rpcConnection: CordaRPCConnection
        private set

    override lateinit var proxy: CordaRPCOps
        private set

    @PostConstruct
    fun initialiseNodeRPCConnection() {
        val rpcAddress = NetworkHostAndPort(host, rpcPort)
        val rpcClient = CordaRPCClient(rpcAddress)
        val rpcConnection = rpcClient.start(username, password)
        proxy = rpcConnection.proxy
    }
}