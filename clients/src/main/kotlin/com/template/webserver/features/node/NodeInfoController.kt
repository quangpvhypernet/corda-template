package com.template.webserver.features.node

import com.template.webserver.models.NodeInfoBO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class NodeInfoController {

    @Autowired
    private lateinit var nodeInfoService: NodeInfoService

    @GetMapping(value = ["me"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun whoAmI(): NodeInfoBO {
        return nodeInfoService.myLegalName
    }

    @GetMapping(value = ["peers"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getPeers(): List<NodeInfoBO> = nodeInfoService.otherPeers
}