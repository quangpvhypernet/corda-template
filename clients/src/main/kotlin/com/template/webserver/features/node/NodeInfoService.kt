package com.template.webserver.features.node

import com.template.webserver.NodeRPCConnection
import com.template.webserver.RPCProxyOwner
import com.template.webserver.features.transfer.SERVICE_NAMES
import com.template.webserver.models.NodeInfoBO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface NodeInfoService {
    val otherPeers: List<NodeInfoBO>
    val myLegalName: NodeInfoBO
}

class MockNodeInfoService : NodeInfoService {
    override val otherPeers: List<NodeInfoBO>
        get() = emptyList()
    override val myLegalName: NodeInfoBO
        get() = NodeInfoBO("", "")

}

@Suppress("unused")
@Service("NodeInfoService")
class NodeInfoServiceImpl(@Autowired private val rpc: NodeRPCConnection) : NodeInfoService {
    private val proxy = (rpc as RPCProxyOwner).proxy
    private val mLegalName = proxy.nodeInfo().legalIdentities.first().name

    override val otherPeers: List<NodeInfoBO>
        get() {
            val nodeInfo = proxy.networkMapSnapshot()
            return nodeInfo
                    .map { NodeInfoBO[it.legalIdentities.first().name] }
                    .filter { it.organisation !in (SERVICE_NAMES + mLegalName.organisation) }
        }
    override val myLegalName: NodeInfoBO
        get() = NodeInfoBO[mLegalName]

}