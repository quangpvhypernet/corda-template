package com.template.webserver.features.transfer

import com.template.flows.TransferNodeToNodeRequester
import com.template.states.TransferNodeToNodeState
import com.template.webserver.NodeRPCConnection
import com.template.webserver.RPCProxyOwner
import com.template.webserver.models.TransferBO
import com.template.webserver.models.TransferTransactionBO
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.startTrackedFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.utilities.getOrThrow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

val SERVICE_NAMES = listOf("Notary", "Network Map Service")

interface TransferService {
    fun getAllTransfers(): List<TransferBO>
    fun getMyTransfers(): List<TransferBO>
    fun createTransfer(value: Int, receiverName: String): TransferTransactionBO
}

class MockTransferServiceImpl : TransferService {
    override fun getAllTransfers(): List<TransferBO> = emptyList()
    override fun getMyTransfers(): List<TransferBO> = emptyList()
    override fun createTransfer(value: Int, receiverName: String) = TransferTransactionBO("000")
}

@Suppress("unused")
@Service("TransferService")
class TransferServiceImpl(@Autowired private val rpc: NodeRPCConnection) : TransferService {
    private val proxy get() = (rpc as RPCProxyOwner).proxy

    override fun getAllTransfers(): List<TransferBO> {
        return proxy.vaultQueryBy<TransferNodeToNodeState>().states.map {
            TransferBO[it]
        }
    }

    override fun getMyTransfers(): List<TransferBO> {
        return proxy.vaultQueryBy<TransferNodeToNodeState>().states.filter {
            it.state.data.sender == proxy.nodeInfo().legalIdentities.first()
        }.map {
            TransferBO[it]
        }
    }

    override fun createTransfer(value: Int, receiverName: String): TransferTransactionBO {

        val receiver = proxy.wellKnownPartyFromX500Name(CordaX500Name.parse(receiverName))
                ?: throw IllegalAccessException("Party named $receiverName cannot be found.\n")

        val signedTx = proxy.startTrackedFlow(::TransferNodeToNodeRequester, value, receiver)
                .returnValue.getOrThrow()

        return TransferTransactionBO[signedTx]
    }
}