package com.template.webserver.provider

import com.template.webserver.MockNodeRPCConnection
import com.template.webserver.NodeRPCConnection
import com.template.webserver.NodeRPCConnectionImpl
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

private const val CORDA_USER_NAME = "config.rpc.username"
private const val CORDA_USER_PASSWORD = "config.rpc.password"
private const val CORDA_NODE_HOST = "config.rpc.host"
private const val CORDA_RPC_PORT = "config.rpc.port"

@Configuration
open class AppProvider {
    //    @Bean
//    open fun jsonConverter(@Autowired rpcConnection: NodeRPCConnection): MappingJackson2HttpMessageConverter {
//        val mapper = JacksonSupport.createDefaultMapper(rpcConnection.proxy)
//        val converter = MappingJackson2HttpMessageConverter()
//        converter.objectMapper = mapper
//        return converter
//    }
    @Bean
    open fun jsonConverter(): MappingJackson2HttpMessageConverter {
        return MappingJackson2HttpMessageConverter()
    }

    /**
     * Wraps an RPC connection to a Corda node.
     *
     * The RPC connection is configured using command line arguments.
     *
     * @param host The host of the node we are connecting to.
     * @param rpcPort The RPC port of the node we are connecting to.
     * @param username The username for logging into the RPC client.
     * @param password The password for logging into the RPC client.
     * @property proxy The RPC proxy.
     */
    @Bean
    open fun nodeConnection(
            @Value("\${$CORDA_NODE_HOST}") host: String,
            @Value("\${$CORDA_USER_NAME}") username: String,
            @Value("\${$CORDA_USER_PASSWORD}") password: String,
            @Value("\${$CORDA_RPC_PORT}") rpcPort: Int
    ): NodeRPCConnection {
        return MockNodeRPCConnection()
    }
}