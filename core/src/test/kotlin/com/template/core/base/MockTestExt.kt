package com.template.core.base

import net.corda.core.identity.CordaX500Name
import net.corda.core.node.NetworkParameters
import net.corda.testing.core.TestIdentity
import net.corda.testing.driver.DriverDSL
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.driver
import net.corda.testing.node.*

fun mockIdentity(
        organisation: String,
        locality: String,
        country: String
) = TestIdentity(CordaX500Name(organisation = organisation, locality = locality, country = country))

fun mockNotary(
        organisation: String,
        locality: String,
        country: String
) = MockNetworkNotarySpec(CordaX500Name(organisation, locality, country))

fun Any.mockServices(packages: List<String> = listOf(
        javaClass.`package`.name
)) = MockServices(packages)

fun Any.mockNetwork(
        packages: List<String> = listOf(javaClass.`package`.name),
        defaultParameters: MockNetworkParameters = MockNetworkParameters(),
        networkSendManuallyPumped: Boolean = defaultParameters.networkSendManuallyPumped,
        threadPerNode: Boolean = defaultParameters.threadPerNode,
        servicePeerAllocationStrategy: InMemoryMessagingNetwork.ServicePeerAllocationStrategy = defaultParameters.servicePeerAllocationStrategy,
        notarySpecs: List<MockNetworkNotarySpec> = defaultParameters.notarySpecs,
        networkParameters: NetworkParameters = defaultParameters.networkParameters
) = MockNetwork(
        packages,
        defaultParameters,
        networkSendManuallyPumped,
        threadPerNode,
        servicePeerAllocationStrategy,
        notarySpecs,
        networkParameters
)

fun MockNetwork.mockNode() = createNode(MockNodeParameters())

fun withDriver(test: DriverDSL.() -> Unit) = driver(
        DriverParameters(isDebug = true, startNodesInProcess = true)
) { test() }