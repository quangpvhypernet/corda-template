package com.template.core.base

import java.lang.reflect.Field


inline fun <reified T> fieldOf(key: String, function: Field.() -> Unit = {}): Field {
    return T::class.java.getDeclaredField(key).also(function)
}