package com.template.core.base

import net.corda.testing.dsl.*
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger

interface ContractTest {
    val services: MockServices

    fun LedgerDSL<TestTransactionDSLInterpreter,
            TestLedgerDSLInterpreter>.case(dsl: TransactionDSL<TransactionDSLInterpreter>.() -> Unit): TestCase {

        return TestCase(this, dsl)
    }

    fun withLedger(script: LedgerDSL<TestTransactionDSLInterpreter, TestLedgerDSLInterpreter>.() -> Unit) {
        services.ledger {
            script()
        }
    }

    fun Verifies.`expect fails with`(message: String): EnforceVerifyOrFail {
        return `fails with`(message)
    }

    fun Verifies.`expect success`(): EnforceVerifyOrFail {
        return verifies()
    }
}