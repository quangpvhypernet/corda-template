package com.template.core.base

import net.corda.testing.dsl.*

class TestCase(
        private val ledgerDSL: LedgerDSL<TestTransactionDSLInterpreter, TestLedgerDSLInterpreter>,
        private val dsl: TransactionDSL<TransactionDSLInterpreter>.() -> Unit
) {
    infix fun `expect fails with`(message: String) {
        ledgerDSL.transaction {
            dsl()
            `fails with`(message)
        }
    }

    fun expectFails(message: String? = null) {
        ledgerDSL.transaction {
            dsl()
            failsWith(message)
        }
    }

    fun expectSuccess() {
        ledgerDSL.transaction {
            dsl()
            verifies()
        }
    }
}