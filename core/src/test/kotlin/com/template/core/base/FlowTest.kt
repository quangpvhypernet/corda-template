package com.template.core.base

import net.corda.core.identity.AbstractParty
import net.corda.core.transactions.SignedTransaction

interface FlowTest {

    fun SignedTransaction.verifySignedBy(vararg party: AbstractParty) {
        assert(requiredSigningKeys == party.map { it.owningKey })
    }
}