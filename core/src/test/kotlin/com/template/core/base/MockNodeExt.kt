package com.template.core.base

import net.corda.core.concurrent.CordaFuture
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.getOrThrow
import net.corda.testing.core.TestIdentity
import net.corda.testing.driver.DriverDSL
import net.corda.testing.driver.NodeHandle
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import java.util.concurrent.Future
import kotlin.reflect.KClass


fun <T> StartedMockNode.start(network: MockNetwork, flow: FlowLogic<T>): CordaFuture<T> {
    val feature = startFlow(flow)
    network.runNetwork()
    return feature
}

fun List<StartedMockNode>.registryFlow(initialFlowClass: KClass<out FlowLogic<*>>) {
    forEach { it.registerInitiatedFlow(initialFlowClass.java) }
}

fun DriverDSL.startNodes(vararg identities: TestIdentity) = identities
        .map { startNode(providedName = it.name) }
        .waitForAll()

fun <T> List<Future<T>>.waitForAll(): List<T> = map { it.getOrThrow() }

fun NodeHandle.resolveName(name: CordaX500Name) = rpc.wellKnownPartyFromX500Name(name)!!.name
