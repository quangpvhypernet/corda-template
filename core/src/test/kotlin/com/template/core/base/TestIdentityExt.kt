package com.template.core.base

import net.corda.testing.core.TestIdentity
import java.security.PublicKey


val List<TestIdentity>.signers: List<PublicKey>
    get() = map { it.publicKey }