package com.template.core.flows

import net.corda.core.flows.FlowLogic
import net.corda.core.utilities.ProgressTracker

val FlowLogic<*>.services get() = serviceHub

fun FlowLogic<*>.step(step: ProgressTracker.Step) {
    progressTracker?.currentStep = step
}