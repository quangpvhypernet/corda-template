package com.template.core.flows

import net.corda.core.flows.FlowSession
import net.corda.core.transactions.SignedTransaction

class SignedSession(val signed: SignedTransaction, vararg val sessions: FlowSession)