package com.template.core.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.CollectSignaturesFlow
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.identity.AbstractParty
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder


fun FlowLogic<*>.signByMe(transaction: TransactionBuilder): SignedTransaction {
    return serviceHub.signInitialTransaction(transaction)
}

@Suspendable
fun FlowLogic<*>.requestPartySign(transaction: SignedTransaction, vararg signer: AbstractParty): SignedSession {
    return requestPartySign(*signer) {
        CollectSignaturesFlow(transaction, it.toSet())
    }
}

@Suspendable
fun FlowLogic<*>.requestPartySign(transaction: SignedTransactionWithOptions, vararg signer: AbstractParty): SignedSession {
    return requestPartySign(*signer) {
        CollectSignaturesFlow(transaction.signedTx, it.toSet(), transaction.optionKeys)
    }
}

@Suspendable
fun FlowLogic<*>.requestPartySign(transaction: SignedTransaction, vararg session: FlowSession): SignedSession {
    val signed = subFlow(CollectSignaturesFlow(transaction, session.toSet()))
    return SignedSession(signed, *session)
}

@Suspendable
private fun FlowLogic<*>.requestPartySign(
        vararg signer: AbstractParty,
        factory: (List<FlowSession>) -> CollectSignaturesFlow
): SignedSession {
    val session = signer.map { initiateFlow(it) }
    val signed = subFlow(factory(session))
    return SignedSession(signed, *session.toTypedArray())
}

@Suspendable
fun FlowLogic<*>.finalSign(signedSession: SignedSession): SignedTransaction {
    return subFlow(FinalityFlow(signedSession.signed, signedSession.sessions.toSet()))
}
