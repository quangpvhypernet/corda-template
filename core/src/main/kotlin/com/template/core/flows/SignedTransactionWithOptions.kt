package com.template.core.flows

import net.corda.core.transactions.SignedTransaction
import java.security.PublicKey

class SignedTransactionWithOptions(
        val signedTx: SignedTransaction,
        val optionKeys: Iterable<PublicKey>? = null
)