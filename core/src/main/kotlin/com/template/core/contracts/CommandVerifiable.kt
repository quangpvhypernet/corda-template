package com.template.core.contracts

import net.corda.core.transactions.LedgerTransaction
import java.security.PublicKey

interface CommandVerifiable {
    fun verify(signers: List<PublicKey>, tx: LedgerTransaction)

    infix fun Boolean.throws(message: String) {
        if (this) throw ContractException(message)
    }

    infix fun String.by(b: Boolean) {
        if (!b) throw ContractException(this)
    }
}