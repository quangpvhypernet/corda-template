package com.template.core.contracts

import net.corda.core.contracts.Contract
import net.corda.core.transactions.LedgerTransaction

object ContractVerifiable : Contract {
    override fun verify(tx: LedgerTransaction) {
        for (command in tx.commands) {
            (command.value as? CommandVerifiable)?.verify(command.signers, tx)
        }
    }
}