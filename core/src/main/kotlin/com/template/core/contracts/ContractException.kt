package com.template.core.contracts

class ContractException(p0: String) : IllegalArgumentException("Contract failed: $p0")