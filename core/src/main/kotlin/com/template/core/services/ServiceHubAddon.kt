package com.template.core.services

import net.corda.core.node.ServiceHub
import net.corda.core.serialization.SerializeAsToken
import kotlin.reflect.KClass


interface ServiceHubAddon : IdentityServiceAddon, MyInfoAddon, NetworkMapCacheAddon {

    val ServiceHub.network get() = networkMapCache
    val ServiceHub.identity get() = identityService
    val ServiceHub.me get() = myInfo

    operator fun <T : SerializeAsToken> ServiceHub.get(serviceClass: KClass<T>): T {
        return cordaService(serviceClass.java)
    }
}
