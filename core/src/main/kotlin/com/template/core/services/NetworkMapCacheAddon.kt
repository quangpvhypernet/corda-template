package com.template.core.services

import net.corda.core.node.services.NetworkMapCache

interface NetworkMapCacheAddon{
    val NetworkMapCache.defaultNotary get() = notaryIdentities.single()
}