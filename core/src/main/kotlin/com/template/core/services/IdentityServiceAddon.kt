package com.template.core.services

import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.IdentityService

interface IdentityServiceAddon {

    fun IdentityService.getParty(nodeName: String): Party {
        return wellKnownPartyFromX500Name(CordaX500Name.parse(nodeName)) ?: error("Not found party $nodeName")
    }
}
