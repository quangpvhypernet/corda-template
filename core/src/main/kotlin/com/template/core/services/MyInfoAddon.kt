package com.template.core.services

import net.corda.core.node.NodeInfo

interface MyInfoAddon{

    val NodeInfo.identity get() = legalIdentities.single()

}