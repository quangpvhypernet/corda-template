package com.template.core.extensions

fun <T> T.toList() = listOf(this)
