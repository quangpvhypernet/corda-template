package com.template.core.extensions

import net.corda.core.identity.AbstractParty
import java.security.PublicKey


val List<AbstractParty>.singers: List<PublicKey>
    get() = map { it.owningKey }