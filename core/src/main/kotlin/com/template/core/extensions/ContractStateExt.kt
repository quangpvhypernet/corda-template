package com.template.core.extensions

import net.corda.core.contracts.ContractState
import java.security.PublicKey


val ContractState.signers: List<PublicKey>
    get() = participants.singers
