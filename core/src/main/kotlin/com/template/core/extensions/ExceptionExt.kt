package com.template.core.extensions


infix fun Boolean.throws(message: String) {
    if (this) throw IllegalAccessException(message)
}