package com.template.core.extensions

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.transactions.LedgerTransaction


inline fun <reified T : ContractState> LedgerTransaction.singleOutput(): T {
    return outputsOfType<T>().single()
}

inline fun <reified T : ContractState> LedgerTransaction.singleInput(): T {
    return inputsOfType<T>().single()
}


inline fun <reified T : CommandData> LedgerTransaction.requireSingleCommand() {
    commands.requireSingleCommand<T>()
}