package com.template

import com.template.core.base.mockIdentity
import com.template.core.base.resolveName
import com.template.core.base.startNodes
import com.template.core.base.withDriver
import org.junit.Test
import kotlin.test.assertEquals

class DriverBasedTest {
    private val bankA = mockIdentity("BankA", "", "GB")
    private val bankB = mockIdentity("BankB", "", "US")

    @Test
    fun `node test`() = withDriver {
        val (partyAHandle, partyBHandle) = startNodes(bankA, bankB)

        // From each node, make an RPC call to retrieve another node's name from the network map, to verify that the
        // nodes have started and can communicate.

        // This is a very basic test: in practice tests would be starting flows, and verifying the states in the vault
        // and other important metrics to ensure that your CorDapp is working as intended.
        assertEquals(bankB.name, partyAHandle.resolveName(bankB.name))
        assertEquals(bankA.name, partyBHandle.resolveName(bankA.name))
    }
}