package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.TransferContract
import com.template.core.extensions.signers
import com.template.core.flows.*
import com.template.core.services.ServiceHubAddon
import com.template.states.TransferNodeToNodeState
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
class TransferNodeToNodeRequester(
        private val value: Int,
        private val receiver: Party
) : FlowLogic<SignedTransaction>(), ServiceHubAddon {

    @Suspendable
    override fun call(): SignedTransaction {
        val state = TransferNodeToNodeState(value, services.me.identity, receiver)
        val transaction = TransactionBuilder(services.network.defaultNotary)
                .addOutputState(state, TransferContract.ID)
                .addCommand(TransferContract.Create(), state.signers)
                .apply { verify(serviceHub) }

        return finalSign(requestPartySign(signByMe(transaction), receiver))
    }
}

@InitiatedBy(TransferNodeToNodeRequester::class)
class TransferNodeToNodeResponder(private val session: FlowSession) : FlowLogic<SignedTransaction>(), ServiceHubAddon {

    @Suspendable
    override fun call(): SignedTransaction {
        return finalReceive(session, signTo(session).id)
    }
}