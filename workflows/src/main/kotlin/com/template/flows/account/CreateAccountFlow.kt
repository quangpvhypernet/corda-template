package com.template.flows.account

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.internal.accountService
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC

@InitiatingFlow
@StartableByRPC
class CreateAccountFlow(private val accountName: String) : FlowLogic<AccountInfo>() {

    @Suspendable
    override fun call(): AccountInfo {
        return serviceHub.accountService.createAccount(accountName).get().state.data
    }
}