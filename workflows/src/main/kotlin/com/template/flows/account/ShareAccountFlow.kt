package com.template.flows.account

import co.paralleluniverse.fibers.Suspendable
import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.internal.accountService
import com.template.core.flows.services
import com.template.core.services.ServiceHubAddon
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC

@InitiatingFlow
@StartableByRPC
class ShareAccountFlow(private val accountName: String, private val node: String) : FlowLogic<AccountInfo>(), ServiceHubAddon {

    @Suspendable
    override fun call(): AccountInfo {
        return serviceHub.accountService.share(accountName, services.identity.getParty(node))
    }
}