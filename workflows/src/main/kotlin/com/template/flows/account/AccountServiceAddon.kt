package com.template.flows.account

import com.r3.corda.lib.accounts.contracts.states.AccountInfo
import com.r3.corda.lib.accounts.workflows.flows.RequestKeyForAccount
import com.r3.corda.lib.accounts.workflows.internal.accountService
import com.r3.corda.lib.accounts.workflows.services.AccountService
import com.r3.corda.lib.accounts.workflows.services.KeyManagementBackedAccountService
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import net.corda.core.identity.PartyAndCertificate
import net.corda.core.node.ServiceHub

fun AccountService.newKey(accountName: String, ourIdentity: PartyAndCertificate): AnonymousParty {
    val account = accountInfo(accountName)[0].state.data
    val accountId = account.identifier.id
    val impl = this as? KeyManagementBackedAccountService
            ?: error("Not found in service hub class ${this.javaClass.simpleName} ")

    return impl.services.keyManagementService.freshKeyAndCert(
            identity = ourIdentity,
            revocationEnabled = false,
            externalId = accountId
    ).let { AnonymousParty(it.owningKey) }
}

fun AccountService.share(actNameShared: String, shareTo: Party): AccountInfo {
    val sharedAccount = ourAccounts().single { it.state.data.name == actNameShared }.state.data
    shareAccountInfoWithParty(sharedAccount.identifier.id, shareTo)
    return sharedAccount
}

fun AccountService.requestKey(flow: FlowLogic<*>, accountName: String): AnonymousParty {
    return flow.subFlow(RequestKeyForAccount(accountInfo(accountName)[0].state.data))
}

operator fun AccountService.get(name: String): AccountInfo {
    return accountInfo(name).single().state.data
}

fun AccountService.create(accountName: String): AccountInfo {
    return createAccount(accountName).get().state.data
}

val ServiceHub.account get() = accountService
