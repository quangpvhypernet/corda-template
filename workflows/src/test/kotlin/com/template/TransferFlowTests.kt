package com.template

import com.template.core.base.*
import com.template.core.extensions.toList
import com.template.flows.TransferNodeToNodeRequester
import com.template.flows.TransferNodeToNodeResponder
import net.corda.core.utilities.getOrThrow
import net.corda.testing.core.singleIdentity
import net.corda.testing.node.MockNetwork
import org.junit.After
import org.junit.Before
import org.junit.Test


class TransferFlowTests {

    private var network: MockNetwork = mockNetwork(
            packages = listOf(
                    "com.template",
                    "com.r3.corda.lib.accounts"
            ),
            notarySpecs = mockNotary("Notary", "London", "GB").toList()
    )
    private val a = network.mockNode()
    private val b = network.mockNode()

    @Before
    fun setup() {
        listOf(a, b).registryFlow(TransferNodeToNodeResponder::class)
        network.runNetwork()
    }

    @Test
    fun `SignedTransaction returned by the flow is signed by the initiator`() {
        val future = a.start(network, TransferNodeToNodeRequester(1, b.info.singleIdentity()))
        val signedTx = future.getOrThrow()
        signedTx.verifySignaturesExcept(b.info.singleIdentity().owningKey)
    }

    @Test
    fun `SignedTransaction returned by the flow is signed by the acceptor`() {
        val future = a.start(network, TransferNodeToNodeRequester(1, b.info.singleIdentity()))
        val signedTx = future.getOrThrow()
        signedTx.verifySignaturesExcept(a.info.singleIdentity().owningKey)
    }

    @Test
    fun `test support account service`() {
//        val service = a.services[AccountSupportService::class]
//        val accountA = service.createAccount("AccountA").getOrThrow().state.data
//        val accountAFromQuery = service.accountInfo("AccountA")[0].state.data
//
//        assert(accountA.identifier.id == accountAFromQuery.identifier.id)
    }

    @After
    fun tearDown() {
        network.stopNodes()
    }
}
