package com.template.contracts

import com.template.core.contracts.CommandVerifiable
import com.template.core.contracts.ContractVerifiable
import com.template.core.extensions.requireSingleCommand
import com.template.core.extensions.singers
import com.template.core.extensions.singleInput
import com.template.core.extensions.singleOutput
import com.template.states.TransferState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.transactions.LedgerTransaction
import java.security.PublicKey

class TransferContract : Contract {

    override fun verify(tx: LedgerTransaction) {
        tx.requireSingleCommand<Command>()
        ContractVerifiable.verify(tx)
    }

    companion object {
        @JvmStatic
        val ID = "com.template.contracts.TransferContract"
    }

    interface Command : CommandData, CommandVerifiable {

        fun TransferState.verifyOutput(signers: List<PublicKey>): TransferState {
            "The sender and the receiver cannot be the same." by (sender != receiver)
            "All of the participants must be signers." by (signers.toSet() == participants.singers.toSet())
            return this
        }
    }

    class Create : Command {
        override fun verify(signers: List<PublicKey>, tx: LedgerTransaction) {
            "Need no input to create transfer" by tx.inputs.isEmpty()
            "Need only one output to create transfer" by (tx.outputs.size == 1)

            val output = tx.singleOutput<TransferState>().verifyOutput(signers)

            with(output) { "The value must be non-negative." by (value > 0) }
        }
    }

    class Modify : Command {
        override fun verify(signers: List<PublicKey>, tx: LedgerTransaction) {
            "Need one input to modify transfer" by (tx.inputs.size == 1)
            "Need one output to modify transfer" by (tx.outputs.size == 1)

            val output = tx.singleOutput<TransferState>().verifyOutput(signers)
            val input = tx.singleInput<TransferState>()

        }
    }
}
