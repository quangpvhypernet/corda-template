package com.template.states

import com.template.contracts.TransferContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.identity.Party

@BelongsToContract(TransferContract::class)
data class TransferNodeToNodeState(
        override val value: Int,
        override val sender: Party,
        override val receiver: Party
) : TransferState
