package com.template.states

import net.corda.core.contracts.ContractState
import net.corda.core.identity.AbstractParty

interface TransferState : ContractState {

    val value: Int
    val sender: AbstractParty
    val receiver: AbstractParty

    override val participants: List<AbstractParty> get() = listOf(sender, receiver)
}