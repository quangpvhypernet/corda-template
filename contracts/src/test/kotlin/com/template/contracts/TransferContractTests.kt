package com.template.contracts

import com.template.core.base.ContractTest
import com.template.core.base.mockIdentity
import com.template.core.base.mockServices
import com.template.core.base.signers
import com.template.states.TransferNodeToNodeState
import net.corda.core.contracts.TypeOnlyCommandData
import org.junit.Test

class TransferContractTests : ContractTest {
    class DummyCommand : TypeOnlyCommandData()

    override val services = mockServices()
    private val megaCorp = mockIdentity("MegaCorp", "London", "GB")
    private val miniCorp = mockIdentity("MiniCorp", "New York", "US")

    @Test
    fun `transaction must have one command`() = withLedger {
        val state = TransferNodeToNodeState(10, megaCorp.party, miniCorp.party)

        case {
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), DummyCommand())
        }.expectFails()

        case {
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())
        }.expectSuccess()
    }

    @Test
    fun `transaction must have no input`() = withLedger {
        val state = TransferNodeToNodeState(10, megaCorp.party, miniCorp.party)
        case {
            input(TransferContract.ID, state)
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())
        } `expect fails with` "Need no input to create transfer"

        case {
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())
        }.expectSuccess()
    }

    @Test
    fun `transaction must have just one output`() = withLedger {
        val state = TransferNodeToNodeState(10, megaCorp.party, miniCorp.party)

        case {
            output(TransferContract.ID, state)
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())

        } `expect fails with` "Need only one output to create transfer"

        case {
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())

        }.expectSuccess()
    }

    @Test
    fun `money output must great than 0`() = withLedger {
        val state = TransferNodeToNodeState(0, megaCorp.party, miniCorp.party)

        case {
            output(TransferContract.ID, state)
            command(listOf(megaCorp.publicKey, miniCorp.publicKey), TransferContract.Create())
        } `expect fails with` "The value must be non-negative."

        case {
            output(TransferContract.ID, state.copy(value = 10))
            command(listOf(megaCorp, miniCorp).signers, TransferContract.Create())
        }.expectSuccess()
    }

}