package com.template.contracts

import com.template.core.base.fieldOf
import com.template.states.TransferNodeToNodeState
import org.junit.Test

class StateTests {
    @Test
    fun `transfer with value in Integer type`() {
        fieldOf<TransferNodeToNodeState>("value") {
            assert(type == Int::class.java)
        }
    }
}